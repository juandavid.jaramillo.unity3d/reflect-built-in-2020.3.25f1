# About

This is a repository for all publicly released packages, tests, and demo projects pertaining to Reflect Viewer.

**Note a valid Reflect license is required to build the Reflect Viewer.**

<a name="Contents"></a>
## Contents

### Supported Unity Versions
This project supports the latest LTS version of Unity 2020.

### Known Issues
This project does not currently support OSX.

<a name="Installation"></a>
## Installation
This repository uses [Git LFS](https://git-lfs.github.com/) so make sure you have LFS installed to get all the files. Unfortunately this means that the large files are also not included in the "Download ZIP" option on Github.

## Building Locally
[Reflect Viewer](ReflectViewer/README.md)
 - Open the ReflectViewer project
 - From the File->Build Settings, select the desired platform and build
 
## MARS and Simulator
You will be required to have MARS installed with the Simulation Environments to use the Device Simulator.  It should be installed automatically as you first open the project.
